// Copyright (c) 2023 Ragil Rynaldo Meyer. All rights reserved.

// Package restclienttest simplifying test restclient by wrapping `*httptest.Server`.
package restclienttest

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestRequest definition how to validate and respond of request.
// All `*Test` fields will be evaluated in sequence when not nil.
// `Responsefunc` field will be prioritized as over `ResponseCode`, `ResponseHeader`, and `ResponseBody`.
type TestRequest struct {
	Name           string
	Test           func(ass *assert.Assertions, r *http.Request) bool
	HeaderTest     func(ass *assert.Assertions, headers http.Header) bool
	FormTest       func(ass *assert.Assertions, forms url.Values) bool
	QueryTest      func(ass *assert.Assertions, queries url.Values) bool
	ResponseFunc   func(ass *assert.Assertions, w http.ResponseWriter)
	ResponseCode   int
	ResponseHeader map[string][]string
	ResponseBody   []byte
}

// RequestHandlers mapped of handler, key is in form or `method:path`.
type RequestHandlers map[string]TestRequest

// TestServer contains dependencies needed for testing.
type TestServer struct {
	*httptest.Server
	handlers  RequestHandlers
	assertion *assert.Assertions
	lock      sync.Mutex
}

// handler main handler, will match request with handler from `RequestHandlers`.
func (ts *TestServer) handler(w http.ResponseWriter, r *http.Request) {
	key := fmt.Sprintf("%s:%s", r.Method, r.URL.Path)

	ts.lock.Lock()
	defer ts.lock.Unlock()

	for k, handler := range ts.handlers {
		if k == key {
			if handler.Test != nil {
				ts.assertion.True(handler.Test(ts.assertion, r), handler.Name+" on `http.Request` test should return true")
			}

			if handler.HeaderTest != nil {
				ts.assertion.True(handler.HeaderTest(ts.assertion, r.Header), handler.Name+" on request header test should return true")
			}

			if handler.FormTest != nil {
				ts.assertion.NoError(r.ParseForm(), handler.Name+"on parse form should be no error")
				ts.assertion.True(handler.FormTest(ts.assertion, r.Form), handler.Name+" on request form values test should return true")
			}

			if handler.QueryTest != nil {
				ts.assertion.True(handler.QueryTest(ts.assertion, r.URL.Query()), handler.Name+" on request query params test should return true")
			}

			if handler.ResponseFunc != nil {
				handler.ResponseFunc(ts.assertion, w)
				return
			}

			for k, val := range handler.ResponseHeader {
				if len(val) > 1 {
					for i, v := range val {
						if i == 0 {
							w.Header().Set(k, v)
							continue
						}
						w.Header().Add(k, v)
					}
				}
				if len(val) < 2 {
					w.Header().Set(k, val[0])
				}
			}
			w.WriteHeader(handler.ResponseCode)
			_, err := w.Write(handler.ResponseBody)
			ts.assertion.NoError(err, handler.Name+" write response should be no error")
			return
		}
	}

	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(404)
	_, err := w.Write([]byte("not found"))
	ts.assertion.NoError(err, " write response should be no error")
}

// AppendHandler append request based on `method` and `path` that will be handled with `testRequest`.
func (ts *TestServer) AppendHandler(method, path string, testRequest TestRequest) {
	key := fmt.Sprintf("%s:%s", method, path)
	ts.lock.Lock()
	defer ts.lock.Unlock()
	ts.handlers[key] = testRequest
}

// Close close the server.
func (ts *TestServer) Close() { ts.Server.Close() }

// NewTestServer create a new `*TestServer` with provided `*testing.T`.
func NewTestServer(t *testing.T) *TestServer {
	obj := &TestServer{
		handlers:  make(RequestHandlers, 1),
		assertion: assert.New(t),
	}
	server := httptest.NewServer(http.HandlerFunc(obj.handler))
	obj.Server = server

	return obj
}
