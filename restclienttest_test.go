package restclienttest

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOps(t *testing.T) {
	ass := assert.New(t)
	server := NewTestServer(t)
	defer server.Close()
	respBody1 := `{"status":"success"}`
	client := server.Client()

	server.AppendHandler(http.MethodGet, "/api/v1/json", TestRequest{
		Name: "Get JSON Request Test",
		Test: func(ass *assert.Assertions, r *http.Request) bool {
			return r.URL.Path == "/api/v1/json" && r.Method == "GET" && r.URL.Query().Get("id") == "1"
		},
		HeaderTest: func(ass *assert.Assertions, header http.Header) bool {
			return header.Get("Authorization") == "Bearer bearer-token"
		},
		QueryTest: func(ass *assert.Assertions, values url.Values) bool {
			return values.Get("id") == "1"
		},
		ResponseFunc: func(ass *assert.Assertions, resp http.ResponseWriter) {
			resp.Header().Set("Content-Type", "application/json")
			resp.Write([]byte(respBody1))
		},
	})

	server.AppendHandler(http.MethodPost, "/api/v1/form", TestRequest{
		Name: "Get JSON Request Test",
		Test: func(ass *assert.Assertions, r *http.Request) bool {
			return r.URL.Path == "/api/v1/form" && r.Method == "POST" && r.URL.Query().Get("id") == "1"
		},
		HeaderTest: func(ass *assert.Assertions, header http.Header) bool {
			return header.Get("Authorization") == "Bearer bearer-token"
		},
		QueryTest: func(ass *assert.Assertions, values url.Values) bool {
			return values.Get("id") == "1"
		},
		FormTest: func(ass *assert.Assertions, forms url.Values) bool {
			return forms.Get("ops") == "test" && forms.Get("pos") == "testing"
		},
		ResponseCode:   200,
		ResponseBody:   []byte(respBody1),
		ResponseHeader: map[string][]string{"Content-Type": {"application/json", "charset=UTF-8"}, "Content-Length": {fmt.Sprint(len([]byte(respBody1)))}},
	})

	req, err := http.NewRequest(http.MethodGet, server.URL+"/api/v1/json?id=1", strings.NewReader(respBody1))
	ass.NoError(err, "no error when creating request")
	req.Header.Set("Authorization", "Bearer bearer-token")
	resp, respErr := client.Do(req)
	ass.NoError(respErr, "no error when send request")
	ass.Equal(200, resp.StatusCode, "status code 200")
	ass.Equal("application/json", resp.Header.Get("Content-Type"), "content type is expected")
	buf, err := ioutil.ReadAll(resp.Body)
	ass.NoError(err, "no error when read body response")
	ass.NoError(resp.Body.Close(), "no error when closing body response")
	ass.Equal(respBody1, string(buf), "response body is expected")

	form := url.Values{"ops": {"test"}, "pos": {"testing"}}
	req2, err2 := http.NewRequest(http.MethodPost, server.URL+"/api/v1/form?id=1", strings.NewReader(form.Encode()))
	ass.NoError(err2, "no error when creating request")
	req2.Header.Set("Authorization", "Bearer bearer-token")
	req2.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp2, respErr2 := client.Do(req2)
	ass.NoError(respErr2, "no error when send request")
	ass.Equal(200, resp2.StatusCode, "status code 200")
	ass.Equal("application/json", resp2.Header.Get("Content-Type"), "content type is expected")
	buf2, err2 := ioutil.ReadAll(resp2.Body)
	ass.NoError(err2, "no error when read body response")
	ass.NoError(resp2.Body.Close(), "no error when closing body response")
	ass.Equal(respBody1, string(buf2), "response body is expected")

	resp3, respErr3 := client.Post(server.URL+"/not-found", "application/json", strings.NewReader(`{"request":"test"}`))
	ass.NoError(respErr3, "no error when send request")
	ass.Equal(404, resp3.StatusCode, "status code 200")
	ass.Equal("text/plain", resp3.Header.Get("Content-Type"), "content type is expected")
	buf3, err3 := ioutil.ReadAll(resp3.Body)
	ass.NoError(err3, "no error when read body response")
	ass.NoError(resp3.Body.Close(), "no error when closing body response")
	ass.Equal("not found", string(buf3), "response body is expected")
}
